import React from 'react'
import ReactDOM from 'react-dom';
import './style.css';
import { Container } from 'react-bootstrap'
// import NavBar from './components/NavBar';
import ProfilePage from './components/ProfilePage';
import NavIcons from './components/NavIcons';

const myComponent = (
    <Container fluid>
        <NavIcons/>
        {/* <NavBar/> */}
        <ProfilePage/>
        
    </Container>
)
const divRoot = document.getElementById('root')
ReactDOM.render(myComponent, divRoot);