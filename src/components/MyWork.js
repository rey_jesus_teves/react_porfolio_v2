import React, { useState } from 'react'
import { Button, Image, Modal } from 'react-bootstrap'
import styles from './MyWork.module.css'

export default function MyWork() {
	
	const [show, setShow] = useState(false)
	const showModal = () => setShow(true)
	const closeModal = () => setShow(false)
	
	const [show2, setShow2] = useState(false)
	const showModal2 = () => setShow2(true)
	const closeModal2 = () => setShow2(false)

	const [show3, setShow3] = useState(false)
	const showModal3 = () => setShow3(true)
	const closeModal3 = () => setShow3(false)

	return (
		
	<section className={styles.section}>
    
    <h1 className={styles.sectionHeader}>MY WORK</h1>
    
    {/* Instruction */}
    <div>
        <p>Select images for more info</p>
    {/* End of Instruction */}    
    </div>

    {/* Project 3 */}
    <div>
    	{/* useState showModal3 */}
        <Button className={styles.modalButton} block onClick={showModal3}>
            {/* Image for Project Preview*/}
            <Image className={styles.images} src='./images1/enrollment.jpg' alt='project' />
        </Button>
        {/* useState show3 and closeModal3 */}
        <Modal show={show3} onHide={closeModal3} centered size='lg'>
            <Modal.Header className={styles.modalHeader}>
                <Modal.Title className={styles.modalTitle}>
                    <h3>Enrollment Website</h3>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles.modalBody}>
                {/* Image for Project Modal*/}
                <Image className={styles.images} src='./images1/enrollment.jpg' alt='project' />
                <h4>
                    Enrollment Website
                </h4>
                <ul>
                    
<li>This is an online enrollment webiste.</li>
<li>The website has a login, registration and enrollment page.</li>
<li>Information displayed is based on the registration data of the student.</li>
<li>The Database is in MongoDB Atlas.</li>
<li>The data is accessed through Express JS and Node JS.</li>
<li>The backend is hosted in Heroku and the Frontend is hosted in Gitlab pages.
</li>








                    {/*Project Link */}
                    <br></br>
                    <p>
                        <a href='https://rey_jesus_teves.gitlab.io/enrollmentwebsite/index.html' target='_blank' rel="noopener noreferrer">
                            <u>Project Link</u>
                        </a>
                    </p>
                </ul>
            </Modal.Body>
        </Modal>
    </div>
    <br />
    {/* End of Project 3 */}
    
    {/* Project 2 */}
    <div>
    	{/* useState showModal2 */}
        <Button className={styles.modalButton} block onClick={showModal2}>
            {/* Image for Project Preview*/}
            <Image className={styles.images} src='./images1/project-2.JPG' alt='project' />
        </Button>
        {/* useState show2 and closeModal2 */}
        <Modal show={show2} onHide={closeModal2} centered size='lg'>
            <Modal.Header className={styles.modalHeader}>
                <Modal.Title className={styles.modalTitle}>
                    <h3>Courses Viewer</h3>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles.modalBody}>
                {/* Image for Project Modal*/}
                <Image className={styles.images} src='./images1/project-2.JPG' alt='project' />
                <h4>
                    Courses Viewer
                </h4>
                <ul>
                    <li>
                        Courses Viewer
                    </li>
                    <li>Elearning Website Programmed in MongoDB Atlas, ExpressJS and NodeJS </li>
                    {/*Project Link */}
                    <br></br>
                    <p>
                        <a href='https://rey_jesus_teves.gitlab.io/coursecard/index.html' target='_blank' rel="noopener noreferrer">
                            <u>Project Link</u>
                        </a>
                    </p>
                </ul>
            </Modal.Body>
        </Modal>
    </div>
    <br />
    {/* End of Project 2 */}
    
    {/* Project 1 */}
    <div>
    	{/* useState showModal */}
        <Button className={styles.modalButton} block onClick={showModal}>
            {/* Image for Project Preview*/}
            <Image className={styles.images} src='./images1/project-1.jpg' alt='project' />
        </Button>
        {/* useState show and closeModal */}
        <Modal show={show} onHide={closeModal} centered size='lg'>
            <Modal.Header className={styles.modalHeader}>
                <Modal.Title className={styles.modalTitle}>
                    <h3>My Portfolio Page</h3>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles.modalBody}>
                {/* Image for Project Modal*/}
                <Image className={styles.images} src='./images1/project-1.jpg' alt='project' />
                <h4>
                    My Portfolio Page
                </h4>
                <ul>
                    <li>
                        Portfolio Page of Rey Jesus Teves
                    </li>
                    <li>This is a portfolio page programmed with HTML, CSS, Bootstrap and Javascript</li>
                    <p>
                        <br></br>
                        <a href='https://rey_jesus_teves.gitlab.io/capstone-1/' target='_blank' rel="noopener noreferrer">
                            <u>Project Link</u>
                        </a>
                    </p>
                </ul>
            </Modal.Body>
        </Modal>
    </div>
    {/* End of Project 1 */}

</section>
	)
}
