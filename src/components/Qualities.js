import React from 'react'
import {
	
	faPeopleArrows,
	faGraduationCap,
	
	
	faCertificate
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './Qualities.module.css'

export default function Qualities() {
	return (
		<section className={styles.section}>
			<h1 className={styles.sectionHeader}>QUALITIES</h1>
			<div className={styles.iconList}>
				
				<div className={styles.iconAndLabel}>
					<FontAwesomeIcon icon={faPeopleArrows} size='4x' fixedWidth />
					<p>Open to feedback</p>
				</div>
				
				<div className={styles.iconAndLabel}>
					<FontAwesomeIcon icon={faGraduationCap} size='4x' fixedWidth />
					<p>Always eager to learn</p>
				</div>
				
			
				<div className={styles.iconAndLabel}>
					<FontAwesomeIcon icon={faCertificate} size='4x' fixedWidth />
					<p>Zuitt Coding Bootcamp Certificate</p>
				</div>
			
			</div>
		</section>
	)
}
