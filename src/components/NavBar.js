import React from 'react'
import { Navbar } from 'react-bootstrap'

export default function NavBar() {
	return (
		<Navbar>
			<div className='navbar-logo text-center'>
				<img src='/logo.png' alt='logo' width='60%' className='img-fluid' />
			<h3>Rey Jesus Teves</h3>	
			</div>
		</Navbar>
	)
}
