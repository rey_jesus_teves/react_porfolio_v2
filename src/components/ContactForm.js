import React from 'react'
import { Form, Button } from 'react-bootstrap'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './ContactForm.module.css'

export default function ContactForm() {

	return (
		<section className={styles.section} id="contact">
			<h1 className={styles.sectionHeader}>CONTACT ME</h1>
            
            <Form
            className={styles.contactForm}
            action="https://formspree.io/f/xayladjw"
			method="POST"
            >
                <Form.Group className={styles.formGroup}>
                    <Form.Label className={styles.formLabel}>Email:</Form.Label>
                    <Form.Control type="email" name="_replyto" />
                </Form.Group>
                <Form.Group className={styles.formGroup}>
                <Form.Label className={styles.formLabel}>Message:</Form.Label>
                    <Form.Control as="textarea" rows={3} name="message" />
                </Form.Group>
                <Button className={styles.formButton} block type="submit">Submit</Button>
            </Form>
			
			<div className={styles.iconList}>
				
				
				<div className={styles.iconAndLabel}>
					<a href='https://www.linkedin.com/in/rey-jesus-teves-62b393100/' target='_blank' rel="noopener noreferrer">
						<FontAwesomeIcon icon={faLinkedin} size='4x' />
						<h3>LinkedIn</h3>
					</a>
				</div>
				
				
			</div>
		
		</section>
	)
}
