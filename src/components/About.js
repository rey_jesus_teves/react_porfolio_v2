import React from 'react'
import styles from './About.module.css'

export default function About() {
	return (
		<section className={styles.section} id="about">
            <div className={styles.sectionHeader}>
			    <h1>ABOUT ME</h1>
                <img src='/images2/rey.jpg' alt='about' />
            </div>
			<br></br>
			<p>
			This website was developed using React JS and hosted in Vercel. I trained as a Full Stack Software Engineer (Next JS and React JS) in Zuitt Programming Bootcamp. If you need a Website Developer, you can contact me through the links on this page.
			</p>

			<p>
			I can design websites using React.js, Javascript, HTML, CSS and Bootstrap. I know how to use Git Command Line Interface and Node Package Manager. I can also create databases using PHP, MySQL, MongoDB Atlas, Express, Node.js. I can host websites using Vercel, Heroku, Cpanel and Gitlab Pages.
			</p>
			
		</section>
	)
}
