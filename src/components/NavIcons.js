import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import { faUserAlt, faList, faPhone } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './NavIcons.module.css'

export default function NavIcons() {
	return (
		// <Navbar className={styles.navContainer} id="nav-footer" fixed='bottom'>



		
		<Navbar className={styles.navContainer} id="nav-footer">


		<Navbar.Toggle aria-controls="basic-navbar-nav" />
  		<Navbar.Collapse id="basic-navbar-nav">

		

			<Nav className={styles.iconList}>
				<Nav.Link className={styles.iconAndLabel} href="#about">
					<FontAwesomeIcon
						icon={faUserAlt}
						size='2x'
						fixedWidth
					/>
                    <p>ABOUT</p>
				</Nav.Link>
				<Nav.Link className={styles.iconAndLabel} href="#skills">
					<FontAwesomeIcon
						icon={faList}
						size='2x'
						fixedWidth
					/>
					<p>SKILLS</p>
				</Nav.Link>
				<Nav.Link className={styles.iconAndLabel} href="#contact">
					<FontAwesomeIcon
						icon={faPhone}
						size='2x'
						fixedWidth
					/>
					<p>CONTACT</p>
				</Nav.Link>
			</Nav>

		</Navbar.Collapse>
		
		</Navbar>

		
		

	)
}
